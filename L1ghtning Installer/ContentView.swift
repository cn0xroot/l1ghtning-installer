//
//  ContentView.swift
//  L1ghtning Installer
//
//  Created by Luke Chambers on 12/1/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI
import SwiftShell

struct ContentView: View {
    @State private var progress: CGFloat = 0
    @State private var current = "Preparing..."
    @State private var failedReason = ""
    @State private var failedDepends = [String]()
    @State private var status: InstallStatus = .working
    
    let depends = [
        ("brew", Dependency(
            install: {
                ShellUtil.shared.ctx.run(bash: "bash <(curl -s https://gitlab.com/snippets/1915962/raw)")
            }
        )),
        ("libusbmuxd", Dependency(
            check: "iproxy",
            brew: "libusbmuxd"
        )),
        ("libssh2", Dependency(
            checkCmd: "ls",
            check: "/usr/local/opt/libssh2/lib/libssh2.1.dylib",
            brew: "libssh2"
        )),
        ("wget", Dependency(
            brew: "wget"
        ))
    ]
    
    var body: some View {
        VStack(spacing: 50) {
            if status == .working {
                VStack {
                    Text("Installing L1ghtning...")
                        .niceText(font: .largeTitle)
                    Text("The ra1n is getting heavier.")
                        .niceText(font: .headline)
                }
                
                VStack {
                    Text(current)
                        .niceText()
                    ProgressBar(progress: $progress)
                }
            } else if status == .failed {
                VStack {
                    Text("Installation Failed")
                        .niceText(font: .largeTitle)
                    Text("The ra1n is slowing down.")
                        .niceText(font: .headline)
                }
                
                VStack {
                    Text(failedReason)
                        .multilineTextAlignment(.center)
                        .niceText()
                    Button(action: {
                        exit(1)
                    }) {
                        Text("Exit")
                            .niceButtonText()
                    }.niceButton(color: .red)
                }
            } else {
                VStack {
                    Text("Installation Succeeded")
                        .niceText(font: .largeTitle)
                    Text("L1ghtning is striking.")
                        .niceText(font: .headline)
                }
                
                VStack {
                    Text(current)
                        .niceText()
                    ProgressBar(progress: $progress)
                    Button(action: {
                        var appDir = URL(fileURLWithPath: Bundle.main.bundleURL.absoluteString)
                        appDir.deleteLastPathComponent()
                        ShellUtil.shared.ctx.run(bash: "open '\(appDir.path)/L1ghtning.app'")
                        exit(0)
                    }) {
                        Text("Launch")
                            .niceButtonText()
                    }.niceButton()
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
        .onAppear(perform: install)
    }
    
    func install() {
        // Preparation
        let parts = CGFloat(depends.count + 3) // Extra steps + 1
        var currentPart: CGFloat = 1
        
        func nextPart(_ label: String? = nil) {
            progress = (currentPart / parts) * 100
            currentPart += 1
            if let newLabel = label {
                current = newLabel
            }
        }
        
        DispatchQueue.global(qos: .background).async {
            // Dependency installation
            self.depends.forEach { dependTuple in
                nextPart("Installing dependencies...")
                
                // Preparation
                let name = dependTuple.0
                let depend = dependTuple.1
                
                // First check
                if !ShellUtil.shared.ctx.run(depend.checkCmd, depend.check ?? name).succeeded {
                    // Installation
                    if let dependInstall = depend.install {
                        dependInstall()
                    }
                    
                    if let brewFormula = depend.brew {
                        ShellUtil.shared.ctx.run("brew", "install", brewFormula)
                    }
                }
                
                // Second check
                if !ShellUtil.shared.ctx.run(depend.checkCmd, depend.check ?? name).succeeded {
                    self.failedDepends.append(name)
                }
            }
            
            // Check for failed dependencies
            if !self.failedDepends.isEmpty {
                self.failedReason = "The following dependencies failed to install: \(self.failedDepends.joined(separator: ", "))"
                self.status = .failed
                return
            }
            
            // Download L1ghtning
            nextPart("Downloading L1ghtning...")
            
            guard let url = URL(string: "https://gitlab.com/snippets/1918520/raw") else {
                self.failedReason = "Failed to retrieve the latest version of L1ghtning."
                self.status = .failed
                return
            }
            
            var latestVersion = ""
            do {
                latestVersion = (try String(contentsOf: url)).trimmingCharacters(in: .whitespacesAndNewlines)
            } catch {
                self.failedReason = "Failed to download the latest version of L1ghtning."
                self.status = .failed
                return
            }
            
            let downloadUrl = "https://gitlab.com/devluke/l1ghtning/raw/master/release/L1ghtning \(latestVersion).zip"
            ShellUtil.shared.ctx.run(bash: "wget -O ~/Downloads/L1ghtning.zip '\(downloadUrl)'")
            ShellUtil.shared.ctx.run(bash: "unzip ~/Downloads/L1ghtning.zip -d ~/Downloads")
            ShellUtil.shared.ctx.run(bash: "rm -rf ~/Downloads/L1ghtning.zip")
            
            // Install L1ghtning
            nextPart("Installing L1ghtning...")
            
            var appDir = URL(fileURLWithPath: Bundle.main.bundleURL.absoluteString)
            appDir.deleteLastPathComponent()
            
            ShellUtil.shared.ctx.run(bash: "mv ~/Downloads/L1ghtning.app '\(appDir.path)'")
            ShellUtil.shared.ctx.run(bash: "rm -rf ~/Downloads/L1ghtning.app")
            
            // Finish up
            nextPart("Done!")
            self.status = .done
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct Dependency {
    var checkCmd: String = "which"
    var check: String? = nil
    var brew: String? = nil
    var install: (() -> Void)? = nil
}

enum FailReason {
    case depend
    case download
}

enum InstallStatus {
    case working
    case failed
    case done
}
